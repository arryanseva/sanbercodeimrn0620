/*Program 1 Looping While
Kamus
data1 dan data2 = string
batas = int
Algoritma */
var data1 = "I love coding"
var data2 = "I will become a mobile developer"
var batas = 2
console.log("LOOPING PERTAMA")
while(batas < 20){
  console.log(batas+" - "+data1)
  batas += 2
  if(batas == 20){
    console.log(batas+" - "+data1)
  }

}
console.log("LOOPING KEDUA")
while(batas > 0) {
  console.log(batas+" - "+data2)
  batas -= 2
}


console.log( )
/* Program 2 Looping For
Kamus
data3,data4,data5 = string
angka = int
Algoritma*/
var data3 = "Santai"
var data4 = "Berkualitas"
var data5 = "I Love Coding"
var angka = 1
for(angka; angka <= 20 ; angka += 1){
  if (angka % 2 == 1 && angka % 3 == 0) {
    console.log(angka +" - " + data5)
  }else if (angka % 2 == 0){
    console.log(angka +" - " + data4)
  }else {
    console.log(angka +" - " + data3)
  }
}

console.log( )

/* Program 3 Membuat Persegi Panjang
Kamus
angka2 dan angka3 = int
pp = string*/
var pp = ""

for(var angka2 = 1; angka2 < 5; angka2++){
  for(var angka3 = 1; angka3 < 9; angka3++){
    pp += "#"
  }
  console.log(pp)
  pp = ""
}

console.log( )

/* Program 4 Membuat Tangga
Kamus
angka4 = int
tangga = string */
var angka4 = 1
var tangga = ""
for(angka4; angka4 < 8 ; angka4 +=1){
  tangga = tangga + "#"
  console.log(tangga)
}

console.log( )

/* Program 5 Membuat Papan
Kamus
pp,hh,catur = string
angka5,angka6 = int
Algoritma */
var pp = " "
var hh = "#"
var catur= ""
var angka5 = 1
var angka6 = 1
for(var angka5 = 1; angka5 < 9 ; angka5 +=1){
  for(var angka6 = 1; angka6 < 9 ; angka6 +=1){
      if(angka5 % 2 == 0 ) {
        if(angka6 % 2 == 0){
          catur += pp
        }
        else{
          catur += hh
        }
      }else{
        if(angka6 % 2 == 0){
          catur += hh
        }
        else{
          catur += pp
        }
      }
  }
  console.log(catur)
  catur = ""
}

