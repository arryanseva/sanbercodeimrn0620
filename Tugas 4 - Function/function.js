/* Program 1 */
// Version 1
function teriak(word){
    return(word = "Halo Sanbers!")
}
  
  console.log(teriak())

 
// Version 2
function teriak(){
    return("Halo Sanbers!")
}
  
console.log(teriak())

// Version 3
var word = function teriak(){
    return("Halo Sanbers!")
  }
  
  console.log(word())


/* Program 2
Kamus
num1,num2,hasilKali = int
Algoritma */
function kalikan(x,y){
    return(x*y)
}
var num1 = 12
var num2 = 4 
var hasilKali = kalikan(num1,num2)
console.log(hasilKali)

/* Program 3 Perkenalan
Kamus
nama,address,hobby,perkenalan = string
age = int
Algoritma*/
function introduce(w,x,y,z){
    return("Nama saya "+w+", umur saya "+x+" tahun, alamat saya di "+y+", dan saya punya hobby yaitu "+z)
  }
  
var nama = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarya"
var hobby = "Gaming!"
  
var perkenalan = introduce(nama,age,address,hobby)
console.log(perkenalan)

