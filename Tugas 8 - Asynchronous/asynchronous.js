function readBooks(time, book, callback ) {
    console.log(`saya membaca ${book.name}`)
    booktime = book.timeSpent
    setTimeout(function(){
        let sisaWaktu = 0
        if(time > book.timeSpent) {
            sisaWaktu = time - book.timeSpent
            console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
            callback(sisaWaktu) //menjalankan function callback
        } else {
            console.log('waktu saya habis')     
        }   
    }, ebel(time,booktime))
}

function ebel(time,booktime){
      if(time > booktime){return booktime}
      else{return time} }


var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var time = 8000
function tc(index){
  if(books.length == index){
    return console.log("semua buku sudah saya baca")
  }
  else{
    readBooks(time,books[index],function(sisa){
      time = sisa
      tc(index+1)
      
    })
  }
}

tc(0)


function readBooksPromise (time, book) {
  z = book.timeSpent
  console.log(`saya mulai membaca ${book.name}`)
  return new Promise( function (resolve, reject){
    setTimeout(function(){
      let sisaWaktu = time - book.timeSpent
      if(sisaWaktu >= 0 ){
          console.log(`saya sudah selesai membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
          resolve(sisaWaktu)
      } else {
          console.log(`saya sudah tidak punya waktu untuk baca ${book.name}`)
          //reject(sisaWaktu) Bisa diaktivin dengan menghilangkan komentar
      }
    }, ebel(time,z))
  })
}

//Code
var waktu = 8000
function prom(index){
  var loop = readBooksPromise(waktu,books[index])
    loop
      .then(function(sisa){
          waktu = sisa
          if((index+1) == books.length){
            console.log("semua buku sudah saya baca")
          }
          else{
            prom(index+1)
          }
      })
      .catch(function(sisa){
          
          console.log("waktu baca habis")
      })
}

prom(0)






































