

/* Program Release 0*/
class Animal {
    constructor(name){
      this._name = name
      this._legs = 4
      this._cold_blooded = false
    }
    get name(){
      return this._name
    }
    get leg(){
      return this._legs
    }
    get cold_blooded() {
      return this._cold_blooded
    }
  }
  var sheep = new Animal("shaun")
  console.log(sheep.name)
  console.log(sheep.leg)
  console.log(sheep.cold_blooded)
  
  /*Program Release 1*/
  class Ape extends Animal {
    constructor(kera){
      super()
      this._kera = kera
      this._leg = 2
    }
    get darah() {return this._cold_blooded}
    get leg() {return this._leg}
    yell(){return "Auooo"}
  }
  
  var sungokong = new Ape("kera sakti")
  console.log(sungokong.yell())
  
  
  class Frog extends Animal {
    constructor(kodok){
      super()
      this._kodok = kodok
    }
    get darah() {return this._cold_blooded}
    get legs() {return this._legs}
    jump(){return "hop hop"}
  }
  
  var kodok = new Frog("buduk")
  console.log(kodok.jump())
  

  /*Program Converting Function to Class */
  class Clock{
    constructor({template}){
      this.template = template
    }
    render(){ // Bagian yang berulang 
      this.date = new Date()
      this.hours = this.date.getHours()
      this.mins = this.date.getMinutes()
      this.secs = this.date.getSeconds()
      this.output = this.template.replace("h",this.hours).replace("m",this.mins).replace("s",this.secs) 
      if(this.hours < 10) this.hours = "0" + this.hours
      if(this.mins < 10) this.mins = "0" + this.mins
      if (this.secs < 10) this.secs = "0" + this.secs
      console.log(this.output)
    }
    stop(){
      clearInterval(this.timer)
    }
    start(){
      this.render()
      this.timer = setInterval(() => this.render(),1000)
    }
  }
  
  
  var clock = new Clock({template: 'h:m:s'})
  clock.start()
    
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  