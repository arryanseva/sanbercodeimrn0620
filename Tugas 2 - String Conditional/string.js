// Program 1 Membuat Kalimat
// Kamus
// word,second,third,fourth,fifth,sixth,seventh = string
// Algoritma
var word = "JavaScript"
var second = "is"
var third = "awesome"
var fourth = "and"
var fifth = "I"
var sixth = "love"
var seventh = "it!"
console.log(word+" "+second+" "+third+" "+fourth+" "+fifth+" "+sixth+" "+seventh)

/* 
Program 2 Menguraikan kalimat
Kamus
Example(1st-8th)Word = string
Algoritma
*/
var sentence = "I am going to be React Native Developer"
var exampleFirstWord = sentence[0]
var exampleSecondWord = sentence[2] + sentence[3]
var exampleThirdWord = sentence[5] + sentence[6] + sentence[7] +sentence[8] +sentence[9]  
var exampleFourthWord = sentence[11] +sentence[12]  
var exampleFifthWord = sentence[14] +sentence[15] 
var exampleSixthWord = sentence[17] +sentence[18] +sentence[19] +sentence[20] +sentence[21]     
var exampleSeventhWord = sentence[23] + sentence[24] +sentence[25] +sentence[26] +sentence[27] +sentence[28]    
var exampleEightWord = sentence[30] +sentence[31] + sentence[32] +sentence[33] +sentence[34] +sentence[35] +sentence[36] +sentence[37] +sentence[38]      
console.log("First Word: " + exampleFirstWord)
console.log("Second Word: " + exampleSecondWord)
console.log("Third Word: " + exampleThirdWord)
console.log("Fourth Word: " + exampleFourthWord)
console.log("Fifth Word: " + exampleFifthWord)
console.log("Sixth Word: " + exampleSixthWord)
console.log("Seventh Word: " + exampleSeventhWord)
console.log("Eigth Word: " + exampleEightWord)


/*Program 3 Menguraikan Kalimat (Substring)*/
//Kamus
//example(1st-5th)Word3 = string
//Algoritma
var sentence2 = "wow Javascript is so cool"
var exampleFirstWord2 = sentence2.substring(0,3)
var secondWord2 = sentence2.substring(4,14)
var thirdWord2 = sentence2.substring(15,17)
var fourthWord2 = sentence2.substring(18,20)
var fifthWord2 = sentence2.substring(21)
console.log("First Word: " + exampleFirstWord2)
console.log("Second Word: " + secondWord2)
console.log("Third Word: " + thirdWord2)
console.log("Fourth Word: " + fourthWord2)
console.log("Fifth Word: " + fifthWord2)

//Program 4 Menguraikan kalimat dan menentukan panjang string
//Kamus
//example(1st-5th)Word3 = string
//Algoritma
var sentence3 = "wow Javascript is so cool"
var exampleFirstWord3 = sentence3.substring(0,3)
var secondWord3 = sentence3.substring(4,14)
var thirdWord3 = sentence3.substring(15,17)
var fourthWord3 = sentence3.substring(18,20)
var fifthWord3 = sentence3.substring(21)

var firstWordLength = exampleFirstWord3.length
var secondWordLength = secondWord3.length
var thirdWordLength = thirdWord3.length
var fourthWordLength = fourthWord3.length
var fifthWordLength = fifthWord3.length

console.log("First Word : " + exampleFirstWord3 + ", with Length: " + firstWordLength )
console.log("Second Word : " + secondWord3 + ", with Length: " + secondWordLength )
console.log("Third Word : " + thirdWord3 + ", with Length: " + thirdWordLength )
console.log("Fourth Word : " + fourthWord3 + ", with Length: " + fourthWordLength )
console.log("Fifth Word : " + fifthWord3 + ", with Length: " + fifthWordLength )

