import React, { Component } from 'react';
import {FlatList, Platform, Text ,Image ,View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class App extends Component {
  render() {
    return (
        <View style={styles.container}>
            <TouchableOpacity>
                <View style={styles.posisi1}>
                    <Image source={require('./images/arrow.png')} style={{height:50,width:50,borderRadius:60}}/>
                </View>
            </TouchableOpacity>
            <View style={styles.set1}>
                <TouchableOpacity>
                    <Image source={require('./images/profile.jpg')} style={{height:80,width:80,borderRadius:60,marginLeft:10,marginRight:10,marginBottom:10}}/>
                </TouchableOpacity>
                <View>
                    <Text style={{fontSize:18}}>Ar Ryan Seva</Text>
                    <Text style={{fontSize:18}}>Web Developer</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row'}}>
                <Text style={{fontSize:16,marginTop:5,marginLeft:10}}>Programming Languages</Text>
                <TouchableOpacity> 
                    <Image source={require('./images/plus.png')} style={{height:25,width:25,borderRadius:60,marginLeft:40,marginRight:10,marginBottom:10,marginTop:5}}/>
                </TouchableOpacity>
            </View>
            <View style={{flexDirection: 'row'}}>
                <Image source={require('./images/js.png')} style={{height:30,width:30,marginLeft:10,marginBottom:5}}/>
                <View>
                    <Text style={styles.font}>Javascript(advanced)</Text>
                    <Text style={styles.font1}>Ability: 96 %</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row'}}>
                <Image source={require('./images/py.png')} style={{height:30,width:30,marginLeft:10,marginBottom:5}}/>
                <View>
                    <Text style={styles.font}>Python(Intermediate)</Text>
                    <Text style={styles.font1}>Ability: 75 %</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row'}}>
                <Image source={require('./images/java.png')} style={{height:30,width:30,marginLeft:10,marginBottom:5}}/>
                <View>
                    <Text style={styles.font}>Java(Basic)</Text>
                    <Text style={styles.font1}>Ability: 40 %</Text>
                </View>
            </View>

            <View style={{flexDirection: 'row'}}>
                <Text style={{fontSize:16,marginTop:5,marginLeft:10}}>Framework/ Library</Text>
                <TouchableOpacity> 
                    <Image source={require('./images/plus.png')} style={{height:25,width:25,borderRadius:60,marginLeft:81,marginRight:10,marginBottom:10,marginTop:5}}/>
                </TouchableOpacity>
            </View>
            <View style={{flexDirection: 'row'}}>
                <Image source={require('./images/react.png')} style={{height:30,width:30,marginLeft:10,marginBottom:5}}/>
                <View>
                    <Text style={styles.font}>React(advanced)</Text>
                    <Text style={styles.font1}>Ability: 91 %</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row'}}>
                <Image source={require('./images/node.png')} style={{height:20,width:50,marginLeft:10,marginBottom:5}}/>
                <View>
                    <Text style={styles.font}>Node(Intermediate)</Text>
                    <Text style={styles.font1}>Ability: 70 %</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row'}}>
                <Image source={require('./images/meteor.png')} style={{height:30,width:30,marginLeft:10,marginBottom:5}}/>
                <View>
                    <Text style={styles.font}>Meteor(Basic)</Text>
                    <Text style={styles.font1}>Ability: 45 %</Text>
                </View>
            </View>

            <View style={{flexDirection: 'row'}}>
                <Text style={{fontSize:16,marginTop:5,marginLeft:10}}>Technologies/Tools</Text>
                <TouchableOpacity> 
                    <Image source={require('./images/plus.png')} style={{height:25,width:25,borderRadius:60,marginLeft:77,marginRight:10,marginBottom:10,marginTop:5}}/>
                </TouchableOpacity>
            </View>
            <View style={{flexDirection: 'row'}}>
                <Image source={require('./images/androidstu.png')} style={{height:30,width:30,marginLeft:10,marginBottom:5}}/>
                <View>
                    <Text style={styles.font}>Android Studio(advanced)</Text>
                    <Text style={styles.font1}>Ability: 90 %</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row'}}>
                <Image source={require('./images/git.png')} style={{height:30,width:30,marginLeft:10,marginBottom:5}}/>
                <View>
                    <Text style={styles.font}>Python(Intermediate)</Text>
                    <Text style={styles.font1}>Ability: 73 %</Text>
                </View>
            </View>
            <View style={{alignItems:'center',paddingTop:20}}>
                <View style={{backgroundColor: '#E5E5E5',height:25,width:200,borderRadius:25}}>
                    <View style={{alignItems:'center'}}>
                        <TouchableOpacity>
                        <Text style={{fontSize:16}}>Reset your skill here</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    );
  } 
}

const styles =StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    posisi1: {
        alignItems: 'flex-end'
    },
    set1: {
        flexDirection: 'row'
    },
    font: {
        fontSize:12,
        marginLeft:20
    },
    font1: {
        fontSize:12
    }
    
})