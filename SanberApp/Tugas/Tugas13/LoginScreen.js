import React, { Component } from 'react';
import {FlatList, Platform, Text ,Image ,View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class App extends Component {
  render() {
    return (
        <View style={styles.container}>
            <View style={styles.posisi}>
                <Image source={require('./images/detektif.jpg')} style={styles.gambar}/>
            </View>
            <View style={styles.tengah}>
                <Text style={styles.teks1}>Welcome To</Text>
                <Text style={styles.teks2}>SanberAPP</Text>
                <Text style={styles.teks3}>"Let's Show Your Programmer Skill Here!"</Text>
                <TouchableOpacity>
                    <View style={styles.bar}>
                        <Text style={styles.teks4}>LOGIN</Text>
                    </View>
                 </TouchableOpacity>
            </View>
            <View style={styles.posisi2}>
                <Text style={styles.teks5}>Username/Email</Text>
                <TouchableOpacity>
                    <View style={styles.bar1}></View>
                </TouchableOpacity>
            </View>
            <View style={styles.posisi2}>
                <Text style={styles.teks5}>Password</Text>
                <TouchableOpacity>
                <View style={styles.bar1}></View>
                </TouchableOpacity>
            </View>
            <View style={styles.posisi2}>
                <Text style={styles.teks5}>Did you have another account? Login with:</Text>
            </View>
            <View style={styles.posisi3}>
                <TouchableOpacity>
                    <Image source={require('./images/facebook-512.png')} style={styles.gambar}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image source={require('./images/image8-2.png')} style={styles.gambar}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image source={require('./images/linked.png')} style={styles.gambar}/>
                </TouchableOpacity>
            </View>
            <View style={styles.posisi2}>
                <Text style={styles.teks5}>Are you haven't any account? Let's Create!</Text>
            </View>
            <View style={styles.tengah}>
                <TouchableOpacity>
                    <View style={styles.bar}>
                        <Text style={styles.teks4}>Register</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
  } 
}

const styles =StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    bar: {
        backgroundColor: '#E5E5E5',
        height : 40,
        width: 200,
        marginTop: 10,
        borderRadius: 25,
        alignItems: 'center'
    },
    bar1: {
        backgroundColor: '#E5E5E5',
        height : 30,
        width: 330,
        marginTop: 5
    },
    teks1: {
        fontSize: 24
    },
    teks2: {
        fontSize: 36,
        color: 'red'
    },
    teks3: {
        fontSize: 18
    },
    teks4: {
        fontSize: 20,
        marginTop: 5
    },
    teks5: {
        fontSize: 16,
        marginBottom: 7
    },
    tengah: {
        alignItems: 'center'
    },
    posisi: {
        alignItems: 'flex-end'
    },
    posisi2: {
        alignItems: 'flex-start',
        paddingTop: 20,
        marginLeft: 10
    },
    posisi3: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    gambar: {
        height:50,
        width:50,
    }
})