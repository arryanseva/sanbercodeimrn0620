import React, { Component } from 'react';
import {FlatList, Platform, Text ,Image ,View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class App extends Component {
  render() {
    return (
        <View style={styles.container}>
            <View style={styles.bar}>
                <TouchableOpacity>
                <View style={{paddingTop:5}}>
                <Icon name="arrow-back" size={25}/>
                </View>
                </TouchableOpacity>
                <TouchableOpacity>
                <Image source={require('./images/2x3.jpg')} style={{height:100,width:100,borderRadius:60}}/>
                </TouchableOpacity>
                <View style={{marginLeft:15}}>
                    <Text style={{fontSize:36,color:'red'}}>SanberApp</Text>
                    <Image source={require('./images/detektif.jpg')} style={{height:50,width:50,borderRadius:20,marginLeft:60}}/>
                </View>
            </View>
            <View style={{alignItems: 'center'}}>
                <Text style={{fontSize:24,paddingTop:15}}>My Account</Text>
            </View>
            <View style={styles.posisi2}>
                <Text style={styles.teks5}>Fullname</Text>
                <TouchableOpacity>
                    <View style={styles.bar1}>
                        <View style={styles.posisi2}>
                            <Text style={{fontSize:16}}>Ar Ryan Shifa Izzati</Text>
                        </View> 
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.posisi2}>
                <Text style={styles.teks5}>Username</Text>
                <TouchableOpacity>
                    <View style={styles.bar1}>
                        <View style={styles.posisi2}>
                            <Text style={{fontSize:16}}>Ar Ryan Seva</Text>
                        </View> 
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.posisi2}>
                <Text style={styles.teks5}>Status</Text>
                <TouchableOpacity>
                    <View style={styles.bar1}>
                        <View style={styles.posisi2}>
                            <Text style={{fontSize:16}}>Fresh Graduate</Text>
                        </View> 
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.posisi2}>
                <Text style={styles.teks5}>Link Portofolio</Text>
                <TouchableOpacity>
                    <View style={styles.bar1}>
                        <View style={styles.posisi2}>
                            <Text style={{fontSize:16}}>Gitlab Account: arryanseva</Text>
                        </View> 
                    </View>
                </TouchableOpacity>
            </View>
            <Text style={{fontSize:16,paddingTop:10, marginLeft:10}}>Social Media :</Text>
            <View style={styles.posisi3}>
                <TouchableOpacity>
                    <Image source={require('./images/facebook-512.png')} style={styles.gambar}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image source={require('./images/ig.png')} style={{height:60,width:60,borderRadius:20}}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image source={require('./images/tele.png')} style={{height:60,width:60}}/>
                </TouchableOpacity>
            </View>
            <View style={styles.posisi3}>
                <Text style={{fontSize:16}}>Ar Ryan Seva</Text>
                <Text style={{fontSize:16}}>arry_seva</Text>
                <Text style={{fontSize:16}}>Ryan</Text>
            </View>
        </View>
    );
  } 
}

const styles =StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    bar: {
        backgroundColor : 'white',
        flexDirection: 'row',
        height:130,
        elevation: 10,
        paddingHorizontal: 15
    },
    posisi2: {
        alignItems: 'flex-start',
        paddingTop: 10,
        marginLeft: 10
    },
    bar1: {
        backgroundColor: '#E5E5E5',
        height : 30,
        width: 330,
        marginTop: 5
    },
    teks5: {
        fontSize: 16,
        marginBottom: 7
    },
    posisi3: {
        paddingTop:15,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    gambar: {
        height:50,
        width:50
    }
    
})