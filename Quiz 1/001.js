//A
function balikString(string){
    balik = ""
    for(var i = string.length-1 ; i>=0 ; i--){
      balik += string[i]
    }
    return(balik)
  }
  
console.log(balikString("abcde"))
console.log(balikString("rusak"))
console.log(balikString("racecar"))
console.log(balikString("haji"))

//B
function palindrome(string){
    balik = ""
    for(var i = string.length-1 ; i>=0 ; i--){
      balik += string[i]
    }
    if (balik == string){
      return true
    }
    else {
      return false
    }
  }
  
console.log(palindrome("kasur rusak"))
console.log(palindrome("haji ijah"))
console.log(palindrome("nabasan"))
console.log(palindrome("nababan"))
console.log(palindrome("jakarta"))

//C
function bandingkan(num1,num2){
    if(num1 == undefined || num2 == undefined){
      if (num1 == undefined){
        return num2
      }else{return num1}
    }
    else if (num1 == undefined && num2 == undefined){
      return -1
    }
    else{
      num1 = parseInt(num1)
      num2 = parseInt(num2)
      if(num1 > 0 &&  num2 > 0){
        if (num1 > num2) {
          return num1
        }
        else if (num1 < num2) {
          return num2 
        }
        else{
          return -1
        }
      }
      else{
        return -1
      }
    }
  }
  
    
console.log(bandingkan(10,15))
console.log(bandingkan(12,12))
console.log(bandingkan(-1,10))
console.log(bandingkan(112,121))
console.log(bandingkan(1))
console.log(bandingkan("112","20"))